#  RippleDrawable

#### 项目介绍
- 项目名称：RippleDrawable
- 所属系列：openharmony的第三方组件适配移植
- 功能：好用的水波纹组件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本: sdk6，DevEco Studio 2.2 Beta2
- 基线版本：Releases 2.0.1

#### 效果演示
<img src="img/RippleDrawable.gif"></img>

#### 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:RippleDrawable:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

java代码调用示例：

```
     mBtn = (FloatingActionButton) findComponentById(ResourceTable.Id_ripple_view);
            mBtn.setBackground(getDrawable2(mBtn,ResourceTable.Graphic_ripple_shape));

 public Element getDrawable2(Component component, int shapeResource){
        return new LollipopDrawable().getShapeElement(component,shapeResource);
    }   
```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
  The MIT License (MIT)
  
  Copyright (c) 2015 Abdullaev Ozodrukh
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

```
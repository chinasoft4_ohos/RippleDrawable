/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dreamers.sample.provider;

import codetail.graphics.drawables.LollipopDrawable;
import dreamers.sample.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;


public class ListXMLProvider extends BaseItemProvider {
    private LayoutScatter layoutScatter;

    /**
     * constructor
     *
     * @param context
     */
    public ListXMLProvider(Context context) {
        this.layoutScatter = LayoutScatter.getInstance(context);
    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        if (component == null) {
            component = layoutScatter.parse(ResourceTable.Layout_item_xml, null, false);

            viewHolder = new ViewHolder();
            viewHolder.image = (Image) component.findComponentById(ResourceTable.Id_img);
            component.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) component.getTag();
        }
        viewHolder.image.setBackground(new LollipopDrawable().getShapeElement(viewHolder.image,ResourceTable.Graphic_ripple_shape));
        return component;
    }

    static class ViewHolder {
        private Image image;
    }
}
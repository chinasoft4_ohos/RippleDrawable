/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dreamers.sample.slice;

import codetail.graphics.drawables.LollipopDrawable;
import com.github.clans.fab.FloatingActionButton;
import dreamers.sample.ResourceTable;
import dreamers.sample.provider.ListXMLProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

import static ohos.agp.components.Component.AXIS_Y;

public class MainAbilitySlice extends AbilitySlice {
    private FloatingActionButton mBtn;
    private ListContainer mListContainerXML;
    private ListXMLProvider listXMLProvider;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mBtn = (FloatingActionButton) findComponentById(ResourceTable.Id_ripple_view);
//        mBtn.setBackground(getDrawable2(mBtn,ResourceTable.Graphic_ripple_shape));
        mListContainerXML = (ListContainer) findComponentById(ResourceTable.Id_list_xml_ripple);
        mListContainerXML.setScrollbarColor(Color.DKGRAY);
        mListContainerXML.setScrollbarThickness(20);
        mListContainerXML.enableScrollBar(AXIS_Y, true);
        mListContainerXML.setScrollbarRoundRect(true);
        mListContainerXML.setScrollbarRadius(10);
        listXMLProvider = new ListXMLProvider(this);
        mListContainerXML.setItemProvider(listXMLProvider);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * 获取背景
     *
     * @param component component
     * @param shapeResource shapeResource
     * @return shapeElement
     */
    public Element getDrawable2(Component component, int shapeResource){
        return new LollipopDrawable().getShapeElement(component,shapeResource);
    }
}
